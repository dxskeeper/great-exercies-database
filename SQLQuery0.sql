﻿--TẠO BẢNG CATEGORY_DETAILS
CREATE TABLE CATEGORY_DETAILS (
	CATEGORY_ID CHAR(5) NOT NULL PRIMARY KEY,
	CATEGORY_DESCRIPTION VARCHAR(20) NOT NULL,
	);
--TẠO BẢNG EMPLOYEE_DETAILS
CREATE TABLE EMPLOYEE_DETAILS (
	EMP_ID	CHAR(10) NOT NULL PRIMARY KEY,
	NAME VARCHAR(30) NOT NULL,
	DESIGNATION VARCHAR(10) NOT NULL,
	WORK_TYPE VARCHAR(10) NOT NULL,
	ADDRESS VARCHAR(50),
	PHONE_NO VARCHAR(13) NOT NULL,
	SALARY MONEY NOT NULL,
	);
--TẠO BẢNG CUSTOMER
CREATE TABLE CUSTOMER (	
	CUSTOMER_ID CHAR(10) NOT NULL PRIMARY KEY,
	NAME VARCHAR(30) NOT NULL,
	ADDRESS VARCHAR(50) NOT NULL,
	PHONE_NO VARCHAR(13),
	NOTE TEXT,
	);
--TẠO BẢNG ITEMS
CREATE TABLE ITEMS (
	ITEM_ID CHAR(5) NOT NULL PRIMARY KEY,
	ITEM_NAME VARCHAR(30) NOT NULL,
	);
--TẠO BẢNG LOGIN_DETAILS
CREATE TABLE LOGIN_DETAILS (
	LOGIN_ID CHAR(10) NOT NULL PRIMARY KEY,
	LOGIN_NAME VARCHAR(30) NOT NULL,
	LOGIN_PASSWORD VARCHAR(50) NOT NULL,
	ROLE VARCHAR(10) NOT NULL,
	CREATED_DATE DATETIME NOT NULL,
	ALLOWED_LOGIN BIT NOT NULL,
	);
--TẠO BẢNG LOGIN_HISTORY
CREATE TABLE LOGIN_HISTORY (
	HISTORY_ID CHAR(10) NOT NULL PRIMARY KEY,
	LOGIN_ID CHAR(10) NOT NULL,
	LOGINDATETIME DATETIME NOT NULL,
	LOGOUTDATETIME DATETIME NOT NULL,
	FOREIGN KEY (LOGIN_ID) REFERENCES LOGIN_DETAILS(LOGIN_ID),
	);
--TẠO BẢNG FOODITEMS_DETAILS
CREATE TABLE FOODITEMS_DETAILS (
	FOODITEM_ID CHAR(10) NOT NULL PRIMARY KEY,
	FOODITEM_NAME VARCHAR(255) NOT NULL,
	CATEGORY_ID CHAR(5) NOT NULL,
	RATE MONEY NOT NULL,
	NOTES TEXT,
	FOREIGN KEY (CATEGORY_ID) REFERENCES CATEGORY_DETAILS(CATEGORY_ID),
	);
--TẠO BẢNG SALESBILL
CREATE TABLE SALESBILL (
	SALESBILL_NO CHAR(10) NOT NULL PRIMARY KEY,
	SALESBILLDATE DATETIME NOT NULL,
	ORDER_ID CHAR(10) NOT NULL,
	CUTOMER_ID CHAR(10) NOT NULL,
	AMOUNT MONEY NOT NULL,
	DISCOUNT NUMERIC(3) NOT NULL,
	FOREIGN KEY (CUTOMER_ID) REFERENCES CUSTOMER(CUSTOMER_ID),
	FOREIGN KEY (ORDER_ID) REFERENCES ORDER_DETAILS(ORDER_ID),
	);
--TẠO BẢNG TABLE_MASTER
CREATE TABLE TABLE_MASTER (
	TABLE_ID CHAR(2) NOT NULL PRIMARY KEY,
	TABLECAPACITY VARCHAR(30) NOT NULL,
	EMP_ID CHAR(10) NOT NULL,
	FOREIGN KEY (EMP_ID) REFERENCES EMPLOYEE_DETAILS(EMP_ID),
	);
---TẠO BẢNG ORDER_DETAILS
CREATE TABLE ORDER_DETAILS (
	ORDER_ID CHAR(10) NOT NULL PRIMARY KEY,
	ORDERDATE DATETIME NOT NULL,
	TABLE_ID CHAR(2) NOT NULL,
	EMP_ID CHAR(10) NOT NULL,
	FOREIGN KEY (TABLE_ID) REFERENCES TABLE_MASTER(TABLE_ID),
	FOREIGN KEY (EMP_ID) REFERENCES EMPLOYEE_DETAILS(EMP_ID),
	);
--TẠO BẢNG PAYMENT
CREATE TABLE PAYMENT(
	PAYMENT_ID CHAR(10) NOT NULL PRIMARY KEY,
	LOGIN_ID CHAR(10) NOT NULL,
	PAYMENTDATE DATETIME NOT NULL,
	SALESBILL_NO CHAR(10) NOT NULL,
	CUSTOMER_ID CHAR(10) NOT NULL,
	PAID_AMOUNT MONEY NOT NULL,
	PAID_MODE MONEY,
	CREDITCARDNO CHAR(16),
	BANKNAME VARCHAR(10),
	BRANCH VARCHAR(10),
	FOREIGN KEY (SALESBILL_NO) REFERENCES SALESBILL(SALESBILL_NO),
	FOREIGN KEY (LOGIN_ID) REFERENCES LOGIN_DETAILS(LOGIN_ID),
	FOREIGN KEY (CUSTOMER_ID) REFERENCES CUSTOMER(CUSTOMER_ID),
	);
--TẠO BẢNG FOODITEMS_ITEMS
CREATE TABLE FOODITEMS_ITEMS(
	FOODITEM_ID CHAR(10) NOT NULL PRIMARY KEY,
	ITEM_ID CHAR(5) NOT NULL,
	FOREIGN KEY (FOODITEM_ID) REFERENCES FOODITEMS_DETAILS(FOODITEM_ID),
	FOREIGN KEY (ITEM_ID) REFERENCES ITEMS(ITEM_ID),
	);
--TẠO BẢNG BILL_FOODITEM
CREATE TABLE BILL_FOODITEM (
	BILLITEMS_ID CHAR(10) NOT NULL PRIMARY KEY,
	SALESBILL_NO CHAR(10) NOT NULL,
	FOODITEM_ID CHAR(10) NOT NULL,
	QUANTITY NUMERIC(2) NOT NULL,
	RATE_PER_ITEMS MONEY NOT NULL,
	AMOUNT MONEY NOT NULL,
	FOREIGN KEY (FOODITEM_ID) REFERENCES FOODITEMS_DETAILS(FOODITEM_ID),
	FOREIGN KEY (SALESBILL_NO) REFERENCES SALESBILL(SALESBILL_NO),
	);
--TẠO BẢNG ORDER_ITEMS
CREATE TABLE ORDER_ITEMS(
	ORDERITEM_ID CHAR(10) NOT NULL PRIMARY KEY,
	ORDER_ID CHAR(10) NOT NULL,
	FOODITEM_ID CHAR(10) NOT NULL,
	QUANTITY NUMERIC(3) NOT NULL,
	RATE_PER_ITEMS MONEY NOT NULL,
	AMOUNT MONEY NOT NULL,
	FOREIGN KEY (ORDER_ID) REFERENCES ORDER_DETAILS(ORDER_ID),
	FOREIGN KEY (FOODITEM_ID) REFERENCES FOODITEMS_DETAILS(FOODITEM_ID),
	);

--NHẬP DỮ LIỆU
INSERT INTO EMPLOYEE_DETAILS 
VALUES
('1','Donna B Bass','manager','fulltime','37,Simpson Street,East Moline,Illinois','218-346-1040','3000'),
('2','Evelyn W Spencern','waiter','fulltime','1157,New Street,Portland,Oregon','541-300-1502','1500'),
('3','Leon T Olson','waiter','partime','ACKWORTH,Iowa','831-350-7180','750'),
('5','Red Bank','cooker','partime','New Jersey','908-491-4755','1250'),
('4','Margit W Ott','cooker','fulltime','2463,Brownton Road,Jackson,Mississippi','662-346-1582','2500'),
('6','Ryan B Yetter','waiter','fulltime','4937,Jett Lane,Hawthorne,California','310-679-7201','1500'),
('7','Robbie F McAfee','cooker','partime','2543,Mcwhorter Road,Iuka,Mississippi','662-423-7475','1250'),
('8','Grace M Pecor','cooker','fulltime','4369,Ridenour Street,Miramar,Florida','786-252-9140','2500'),
('9','Micheal M Tse','waiter','partime','4622,Science Center Drive,Boise,Idaho','208-571-8826','750'),
('10','Avis N Stubblefield','waiter','partime','2788,Progress Way,Waterloo,Iowa','319-833-1836','750'),
('11','Leora H Moreno','waiter','fulltime','4579,Alexander Drive,Argyle,Texas','940-464-5916','1500');
INSERT INTO CUSTOMER 
VALUES
('1','Brad M Edgerton','3886,Woodland Avenue,New Orleans,Louisiana','985-605-7921',''),
('2','John C Berger','2792,Oakmound Drive,JACKSON,Mississippi','773-409-7087',''),
('3','Michelle P Snodgrass','4437,Lonely Oak Drive,Mobile,Alabama','251-460-9302',''),
('4','Cathleen E Santos','1455,Cardinal Lane,GREENSBORO,Vermont','217-255-1422',''),
('5','Bernita J Lewis','1113,Berry Street,Hugo,Colorado','fulltime',''),
('6','Ronald A Gutierrez','3902,Corpening Drive,Pontiac,Michigan','248-452-0300',''),
('7','Janice M Boucher','1868,Java Lane,North Augusta,South Carolina','803-640-6522',''),
('8','Michael A Weintraub','3327,Counts Lane,IRWINTON,Georgia','859-962-8154',''),
('9','Thelma R Simental','995,Willow Oaks Lane,Sugartown,Louisiana','337-328-3689',''),
('10','Pearl P McConnell','900,Burton Avenue,Memphis,Tennessee','901-683-3366',''),
('11','Rebecca J Griffin','3712,Upland Avenue,Willard,Ohio','419-933-1821','');
INSERT INTO CATEGORY_DETAILS
VALUES
('1','CHICKEN'),
('2','MEAT'),
('3','VEAL'),
('4','FISH'),
('5','EGG');
INSERT INTO FOODITEMS_DETAILS
VALUES
('1','Chicken and pancetta spaghetti','1','23','Spagetti topped with a blend of corn-fed chicken and smoked pancetta'),
('2','Chicken and bacon pasta','1','44','resh egg pasta in a sauce made from free range chicken and streaky bacon'),
('3','Chicken and turkey burgers','1','50','Tasty burgers made from corn-fed chicken and smoked turkey, served in a roll'),
('4','Sultana tart with chicken sauce','1','78','A rich pastry case filled with sultana and served with chicken sauce'),
('5','Chicken and pork madras','1','54','Medium-hot madras made with corn-fed chicken and pork'),
('6','Chicken and venison korma','1','46','Creamy korma made with corn-fed chicken and venison'),
('7','Chicken and beef vindaloo','1','63','Hot vindaloo made with free range chicken and beef'),
('8','Chicken and game pie','1','32','A puff pasty case filled with free range chicken and game'),
('9','Roast beef','2','88','Roast beef with all the trimmings'),
('10','Beef and pasta casserole','2','50','Beef and farfalle pasta stewed'),
('11','Beef and sausage stew','2','63','Beef and chunky sausage stewed'),
('12','Beef and aubergine burgers','2','63','Beef and chunky sausage stewed'),
('13','Beef and squash madras','2','50','Succulent burgers made from beef and chargrilled aubergine, served in a roll'),
('14','Beef and squash madras','2','63','Medium-hot madras made with beef and pattypan squash'),
('15','Beef and pepper korma','2','88','Mild korma made with beef and green pepper'),
('16','Beef and crab vindaloo','2','88','Spicy vindaloo made with beef and crab'),
('17','Veal and sweetcorn parcels','3','88','Thin filo pastry cases stuffed with veal and creamed sweetcorn'),
('18','Veal and pheasant gyoza','3','88','Thin pastry cases stuffed with veal and pheasant'),
('19','Veal and spinach wontons','3','50','Thin wonton cases stuffed with veal and baby spinach'),
('20','Veal and horseradish dumplings','3','50','Thin pastry cases stuffed with veal and fresh horseradish'),
('21','Veal and pork stew','3','50','Veal and pork stewed'),
('22','Veal salad with garlic dressing','3','46','A mouth-watering veal salad served with garlic dressing'),
('23','Gougons fish','4','78','Crispy, breaded gougons of fish'),
('24','Fish salad with garlic dressing','4','78','A mouth-watering fish salad served with garlic dressing'),
('25','Fish and chicken curry','4','50','Mild curry made with fish and corn-fed chicken'),
('26','Fish sandwich with onion relish','4','46','Fish and onion relish served between slices of fresh bread'),
('27','Fish and egg toastie','4','46','Crisp slices of bread filled with fish and free range eggs'),
('28','Fish and lettuce panini','4','50','A hot, pressed panini filled with fish and little gem lettuce'),
('29','Fish and lettuce ciabatta','4','32','Warm ciabatta filled with fish and iceberg lettuce'),
('30','Chocolate and blackcurrant crepes','5','32','Fluffy crepes filled with milk chocolate and fresh blackcurrant'),
('31',' Hazelnut and pecan muffins','5','','Moist muffins made with hazelnut and pecan'),
('32','Cardamom and oatbran muffins','5','50','Fluffy muffins made with green cardamom and oatbran'),
('33','Squash and cheese muffins','5','50','Fluffy muffins made with butternut squash and mature cheese'),
('34','Stilton and rosemary muffins','5','32','Fluffy muffins made with stilton and fresh rosemary'),
('35','Honey and lemon muffins','5','78','Fluffy muffins made with acacia honey and fresh lemon');
INSERT INTO ITEMS
VALUES
('1','tomato'),
('2','passata '),
('3','onion'),
('4','garlic'),
('5','spagetti'),
('6','oregano'),
('7','black pepper'),
('8','lobster'),
('9','mango'),
('10','sausage'),
('11','cumin'),
('12','pasta'),
('13','tumeric'),
('14','flour'),
('15','yeast'),
('16','water'),
('17','salt'),
('18','sugar'),
('19','oil'),
('20','coriander'),
('21','ginger'),
('22','chilli'),
('23','mint '),
('24','peppers'),
('25','butter'),
('26','cream'),
('27','tumeric'),
('28','mustard seeds'),
('29','cardamon'),
('30','cinnamon'),
('31','wine vinegar'),
('32','cayenne'),
('33','fish'),
('34','beef'),
('35','veal'),
('36','egg'),
('37','cucumber'),
('38','lettuce'),
('39','cress'),
('40','garlic'),
('41','curry leaf'),
('42','green chilli'),
('43','plantain'),
('44','tofu'),
('45','ricotta'),
('46','sweetcorn'),
('47','cod'),
('48','cardamom'),
('49','mushroom'),
('50','mushroom');
INSERT INTO LOGIN_DETAILS
VALUES
('8724922718','elliott_mi1973','beuk6Zue3d','manager','02/11/2016','1'), 
('5653651247','Mad_Kamikaze','yishowoPh3tie','waiter','01/25/2017','1'),
('4209043368','Mad_Kamikaze','yishowoPh3tie','waiter','02/01/2017','1'),
('5125591329','dominic','Ah7shi3aiCei','waiter','04/12/2017','1'),
('1791339983','Melenth','ed0Jah0goo6','waiter','01/28/2018','1'),
('4209043368','dombo813','bag2ea7bohZ','cooker','07/03/2018','1'),
('6267676815','GWBENV','bag2ea7bohZ','cooker','11/07/2018','1'),
('8817053883','oriflamme','Eeph7tee8','cooker','12/18/2018','1'),
('5027619356','caralho','Fremont','cooker','01/05/2019','1'),
('0554126387','toy','ee1Eizeew','waiter','01/08/2019','1'),
('3627167440','joesph','eesoh6OhY','waiter','08/22/2019','1');
INSERT INTO LOGIN_HISTORY
VALUES
('6676698459','8724922718','01/11/2017','01/03/2018'),
('3475966821','5653651247','01/31/2017','01/07/2018'),
('1269949933','4209043368','02/01/2017','03/14/2018'),
('3878248678','5125591329','02/21/2017','04/26/2018'),
('5172085999','5125591329','03/10/2017','06/16/2018'),
('2092580042','8724922718','03/21/2017','07/19/2018'),
('4244509166','1791339983','03/28/2017','07/27/2018'),
('7498327693','4368676012','04/27/2017','08/01/2018'),
('7715440395','5027619356','01/03/2018','08/01/2018'),
('5615614243','3627167440','08/03/2018','01/02/2019'),
('6023757346','4209043368','09/13/2018','01/02/2019'),
('6294342667','0554126387','09/26/2018','02/04/2019'),
('3710862945','8724922718','01/02/2019','02/10/2019'),
('4450917469','3627167440','02/27/2019','04/15/2019'),
('0236292195','8724922718','02/27/2019','03/22/2019'),
('9390163735','1791339983','02/27/2019','04/15/2019'),
('1125105675','4209043368','05/04/2019','06/23/2019'),
('0126978694','4209043368','08/18/2019','08/31/2019');
INSERT INTO TABLE_MASTER
VALUES
('1','4','9'),
('2','2','6'),
('3','9','9'),
('4','7','9'),
('5','4','9'),
('6','8','6'),
('7','1','6'),
('8','6','10'),
('9','3','10'),
('10','2','6'),
('11','5','6'),
('12','7','11'),
('13','4','11'),
('14','3','2'),
('15','8','3'),
('16','9','6'),
('17','12','6'),
('18','4','2'),
('19','2','2'),
('20','5','3');
INSERT INTO ORDER_DETAILS
VALUES
('9818214027','01/03/2018','1','9'),
('3762023056','01/03/2018','1','9'),
('8484280951','01/07/2018','12','11'),
('3654733896','01/07/2018','19','2'),
('0866278596','01/16/2018','11','6'),
('1443156757','01/27/2018','5','9'),
('2151616378','01/28/2018','8','10'),
('9715865937','01/30/2018','3','9'),
('0107406703','01/30/2018','9','10'),
('3170658265','02/05/2018','20','3');
INSERT INTO ORDER_ITEMS
VALUES
('9535288544','0107406703','23','1','78','78'),
('1738372068','2151616378','1','1','23','23'),
('5466689821','1443156757','4','2','78','156'),
('9015183754','9715865937','12','1','63','63'),
('2713610572','3170658265','15','3','88','264'),
('4408490989','3654733896','16','1','88','88'),
('7843371186','8484280951','5','2','54','108'),
('7449938754','0866278596','9','2','88','176'),
('4153183025','3762023056','17','3','88','264'),
('5800691842','9818214027','29','2','32','64');
INSERT INTO SALESBILL
VALUES
('8725939999','01/03/2018','9818214027','5','32.00','0'),
('1416846947','01/03/2018','3762023056','5','88.00','0'),
('8152469765','01/07/2018','8484280951','7','54.00','0'),
('2333787374','01/07/2018','3654733896','7','88.00','0'),
('6407373463','01/16/2018','0866278596','2','88.00','0'),
('0162334583','01/27/2018','1443156757','1','78.00','0'),
('7470964573','01/28/2018','2151616378','8','23.00','0'),
('0784759271','01/30/2018','9715865937','10','23.00','0'),
('3405536760','01/30/2018','0107406703','10','78.00','0'),
('2107962658','02/05/2018','3170658265','1','88.00','0');

--LIỆT KÊ TẤT CẢ CÁC KHÁCH HÀNG
SELECT	* FROM CUSTOMER;
--LIỆT KÊ TẤT CẢ MÓN ĂN 
SELECT	* FROM FOODITEMS_DETAILS;
--LIỆT KÊ TẤT CẢ ORDERITEM
SELECT	* FROM CUSTOMER;
--LIỆT KÊ TẤT CẢ PAYMENT
SELECT	* FROM PAYMENT;
--LIỆT KÊ TẤT CẢ SALESBILL
SELECT	* FROM SALESBILL;
--LIỆT KÊ TẤT CẢ BÀN ĂN
SELECT	* FROM TABLE_MASTER;
--LIỆT KÊ TẤT CẢ FOODITEMS
SELECT	* FROM FOODITEMS_DETAILS;
--LIỆT KÊ CHI TIẾT KHÁCH HÀNG ĐÃ THANH TOÁN TRONG NGÀY 12/2/2018
SELECT * FROM PAYMENT WHERE PAYMENTDATE = '12/2/2017';
--LIỆT KÊ CHI TIẾT CÁC HÓA ĐƠN TRONG NGÀY 3/4/2018
SELECT * FROM SALESBILL WHERE SALESBILLDATE ='3/4/2018';
--THỐNG KÊ TỔNG TIỀN ĐÃ NHẬN ĐƯỢC TỪ KHÁCH HÀNG
SELECT SUM(PAID_MODE) AS SUM_PAYMODE FROM PAYMENT;
--THỐNG KÊ KHÁCH HÀNG NÀO ĂN MÓN ĂN NÀO ĐẮT TIỀN NHẤT
SELECT CI.*,FD.FOODITEM_NAME FROM  BILL_FOODITEM BF
INNER JOIN SALESBILL SB ON BF.SALESBILL_NO = SB.SALESBILL_NO
INNER JOIN CUSTOMER CI ON CI.CUSTOMER_ID = SB.CUTOMER_ID
LEFT JOIN FOODITEMS_DETAILS FD ON FD.FOODITEM_ID = BF.FOODITEM_ID
WHERE BF.RATE_PER_ITEMS = (
	SELECT MAX(RATE_PER_ITEMS) FROM BILL_FOODITEM
	);

--THỐNG KÊ SỐ LƯỢNG THỨC ĂN CÓ GIÁ THẤP HƠN 55
SELECT COUNT(FOODITEM_ID) AS COUNT_FOOD FROM FOODITEMS_DETAILS
WHERE RATE < 55;
--THỐNG KÊ SỐ LƯỢNG NHÂN VIÊN WAITER ĐÃ PHỤC VỤ BÀN CÓ SỐ LƯỢNG NGƯỜI SẮP XẾP TĂNG DẦN CÓ TỔNG SỐ TIỀN HÓA ĐƠN THẤP NHẤT
SELECT ED.*,TM.TABLE_ID,TM.TABLECAPACITY FROM ORDER_DETAILS OD
INNER JOIN EMPLOYEE_DETAILS ED ON ED.EMP_ID = OD.EMP_ID
INNER JOIN TABLE_MASTER TM ON TM.TABLE_ID = OD.TABLE_ID
INNER JOIN ORDER_ITEMS OI ON OI.ORDER_ID = OD.ORDER_ID
WHERE AMOUNT = (
	SELECT MIN(AMOUNT) FROM ORDER_ITEMS
	)
ORDER BY TM.TABLECAPACITY ASC;
--THỐNG KÊ NGƯỜI DÙNG CÓ LỊCH SỬ ĐĂNG NHẬP ĐẴ ĐƯỢC 27 NGÀY TỪ NGÀY 12/3/2017 ĐẾN NGÀY 22/6/2018	
SELECT LD.* FROM LOGIN_HISTORY LH
INNER JOIN LOGIN_DETAILS LD ON LD.LOGIN_ID = LH.LOGIN_ID
WHERE DATEDIFF(DAY,LH.LOGINDATETIME,LH.LOGOUTDATETIME) =27
--LIỆT KÊ CHI TIẾT TẤT CẢ DANH MỤC MÓN ĂN CÓ PHÂN LOẠI LÀ EGG
SELECT FD.* FROM FOODITEMS_DETAILS FD
INNER JOIN CATEGORY_DETAILS CD ON CD.CATEGORY_ID = FD.CATEGORY_ID
WHERE CD.CATEGORY_DESCRIPTION = 'EGG';
--THỐNG KÊ THỜI GIAN TRUY CẬP CỦA NGƯỜI DÙNG
SELECT HISTORY_ID,LOGIN_ID,DATEDIFF(DAY,LOGINDATETIME,LOGOUTDATETIME) AS ACCESS_TIME FROM LOGIN_HISTORY;
--THỐNG KÊ NHÂN VIÊN PHỤC VỤ ORDER NGÀY 16/04/2017
SELECT ED.* FROM EMPLOYEE_DETAILS ED
INNER JOIN ORDER_DETAILS OD ON OD.EMP_ID= ED.EMP_ID
WHERE OD.ORDERDATE = '16/04/2017';
